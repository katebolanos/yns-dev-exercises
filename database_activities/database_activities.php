<?php include("db.php"); ?>
<!DOCTYPE HTML>
<html>
    <head>
        <title>DATABASE ACTIVTIES</title>
    </head>
    <style>
        table, td, th, tr {
            border: 1px solid black;
        }
    </style>
    <body>
        <!-- EXERCISE SELECT STATEMENT 1 -->
        <h4>EXERCISE SELECT STATEMENT 1</h4>
        <table>
            <tr>
                <th>ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Middle Name</th>
                <th>Birth Date</th>
                <th>Department ID</th>
                <th>Hire Date</th>
                <th>Boss ID</th>
            </tr>
            <?php
                $query = "SELECT * FROM employees WHERE last_name LIKE 'K%'";
                $result = mysqli_query($db, $query);
                if (mysqli_num_rows($result) > 0){
                    while ($row = mysqli_fetch_assoc($result)){        
            ?>  
                        <tr>
                        <?php foreach ($row as $val) { ?>
                            <td><?php echo $val; ?></td>
                        <?php } ?>
                        </tr>
            <?php
                    }
                } else {
                    echo "0 results";
                }
            ?>
        </table><br>
        <!-- EXERCISE SELECT STATEMENT 2 -->
        <h4>EXERCISE SELECT STATEMENT 2</h4>
        <table>
            <tr>
                <th>ID</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Middle Name</th>
                <th>Birth Date</th>
                <th>Department ID</th>
                <th>Hire Date</th>
                <th>Boss ID</th>
            </tr>
            <?php
                $query = "SELECT * FROM employees WHERE last_name LIKE '%i'";
                $result = mysqli_query($db, $query);
                if (mysqli_num_rows($result) > 0){
                    while ($row = mysqli_fetch_assoc($result)){        
            ?>  
                        <tr>
                        <?php foreach ($row as $val) { ?>
                            <td><?php echo $val; ?></td>
                        <?php } ?>
                        </tr>
            <?php
                    }
                } else {
                    echo "0 results";
                }
            ?>
        </table><br>
        <!-- EXERCISE SELECT STATEMENT 3 -->
        <h4>EXERCISE SELECT STATEMENT 3</h4>
        <table>
            <tr>
                <th>Full Name</th>
                <th>Hire Date</th>
                
            </tr>
            <?php
                $query = "SELECT CONCAT(first_name, ' ', middle_name, '. ', last_name), hire_date AS full_name FROM employees WHERE hire_date BETWEEN '2015-01-01' AND '2015-3-31' ORDER BY hire_date ASC";
                $result = mysqli_query($db, $query);
                if (mysqli_num_rows($result) > 0){
                    while ($row = mysqli_fetch_assoc($result)){        
            ?>  
                        <tr>
                        <?php foreach ($row as $val) { ?>
                            <td><?php echo $val; ?></td>
                        <?php } ?>
                        </tr>
            <?php
                    }
                } else {
                    echo "0 results";
                }
            ?>
        </table><br>
        <!-- EXERCISE SELECT STATEMENT 4 -->
        <h4>EXERCISE SELECT STATEMENT 4</h4>
        <table>
            <tr>
                <th>Employee</th>
                <th>Boss</th>
            </tr>
            <?php
                $query = "SELECT employee_table.last_name AS employee, boss_table.last_name AS boss FROM employees AS employee_table INNER JOIN employees AS boss_table ON employee_table.boss_id = boss_table.employee_id";
                $result = mysqli_query($db, $query);
                if (mysqli_num_rows($result) > 0){
                    while ($row = mysqli_fetch_assoc($result)){        
            ?>  
                        <tr>
                        <?php foreach ($row as $val) { ?>
                            <td><?php echo $val; ?></td>
                        <?php } ?>
                        </tr>
            <?php
                    }
                } else {
                    echo "0 results";
                }
            ?>
        </table><br>
        <!-- EXERCISE SELECT STATEMENT 5 -->
        <h4>EXERCISE SELECT STATEMENT 5</h4>
        <table>
            <tr>
                <th>Last Name</th>
            </tr>
            <?php
                $query = "SELECT employees.last_name FROM employees INNER JOIN department ON employees.department_id = department.department_id WHERE department.name = 'Sales'";
                $result = mysqli_query($db, $query);
                if (mysqli_num_rows($result) > 0){
                    while ($row = mysqli_fetch_assoc($result)){        
            ?>  
                        <tr>
                        <?php foreach ($row as $val) { ?>
                            <td><?php echo $val; ?></td>
                        <?php } ?>
                        </tr>
            <?php
                    }
                } else {
                    echo "0 results";
                }
            ?>
        </table><br>
        <!-- EXERCISE SELECT STATEMENT 6 -->
        <h4>EXERCISE SELECT STATEMENT 6</h4>
        <table>
            <tr>
                <th>Count has middle name</th>
            </tr>
            <?php
                $query = "SELECT COUNT(*) AS count FROM employees WHERE middle_name IS NOT NULL";
                $result = mysqli_query($db, $query);
                if (mysqli_num_rows($result) > 0){
                    while ($row = mysqli_fetch_assoc($result)){        
            ?>  
                        <tr>
                        <?php foreach ($row as $val) { ?>
                            <td><?php echo $val; ?></td>
                        <?php } ?>
                        </tr>
            <?php
                    }
                } else {
                    echo "0 results";
                }
            ?>
        </table><br>
        <!-- EXERCISE SELECT STATEMENT 7 -->
        <h4>EXERCISE SELECT STATEMENT 7</h4>
        <table>
            <tr>
                <th>Department Name</th>
                <th>Count</th>
            </tr>
            <?php
                $query = "SELECT department.name AS Department, COUNT(*) AS count FROM employees INNER JOIN department ON employees.department_id = department.department_id GROUP BY department.name";
                $result = mysqli_query($db, $query);
                if (mysqli_num_rows($result) > 0){
                    while ($row = mysqli_fetch_assoc($result)){        
            ?>  
                        <tr>
                        <?php foreach ($row as $val) { ?>
                            <td><?php echo $val; ?></td>
                        <?php } ?>
                        </tr>
            <?php
                    }
                } else {
                    echo "0 results";
                }
            ?>
        </table><br>
        <!-- EXERCISE SELECT STATEMENT 8 -->
        <h4>EXERCISE SELECT STATEMENT 8</h4>
        <table>
            <tr>
                <th>Name</th>
                <th>Hire Date</th>
            </tr>
            <?php
                $query = "SELECT concat(first_name, ' ', last_name ), hire_date FROM employees WHERE hire_date=(SELECT max(hire_date) FROM employees)";
                $result = mysqli_query($db, $query);
                if (mysqli_num_rows($result) > 0){
                    while ($row = mysqli_fetch_assoc($result)){        
            ?>  
                        <tr>
                        <?php foreach ($row as $val) { ?>
                            <td><?php echo $val; ?></td>
                        <?php } ?>
                        </tr>
            <?php
                    }
                } else {
                    echo "0 results";
                }
            ?>
        </table><br>
        <!-- EXERCISE SELECT STATEMENT 9 -->
        <h4>EXERCISE SELECT STATEMENT 9</h4>
        <table>
            <tr>
                <th>Department Name</th>
            </tr>
            <?php
                $query = "SELECT name FROM department LEFT JOIN employees ON department.department_id = employees.department_id WHERE employees.department_id IS NULL";
                $result = mysqli_query($db, $query);
                if (mysqli_num_rows($result) > 0){
                    while ($row = mysqli_fetch_assoc($result)){        
            ?>  
                        <tr>
                        <?php foreach ($row as $val) { ?>
                            <td><?php echo $val; ?></td>
                        <?php } ?>
                        </tr>
            <?php
                    }
                } else {
                    echo "0 results";
                }
            ?>
        </table><br>
        <!-- EXERCISE SELECT STATEMENT 10 -->
        <h4>EXERCISE SELECT STATEMENT 10</h4>
        <table>
            <tr>
                <th>First Name</th>
                <th>Middle Name</th>
                <th>Last Name</th>
            </tr>
            <?php
                $query = "SELECT first_name, middle_name, last_name FROM employees INNER JOIN employee_positions ON employees.employee_id = employee_positions.employee_id HAVING COUNT(employee_positions.employee_id)";
                $result = mysqli_query($db, $query);
                if (mysqli_num_rows($result) > 0){
                    while ($row = mysqli_fetch_assoc($result)){        
            ?>  
                        <tr>
                        <?php foreach ($row as $val) { ?>
                            <td><?php echo $val; ?></td>
                        <?php } ?>
                        </tr>
            <?php
                    }
                } else {
                    echo "0 results";
                }
            ?>
        </table><br>

    </body>
</html>