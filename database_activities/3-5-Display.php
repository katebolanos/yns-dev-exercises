<?php include("db.php"); ?>
<!DOCTYPE HTML>
<html>
    <head>
        <title>DATABASE ACTIVTIES</title>
    </head>
    <style>
        table, td, th, tr {
            border: 1px solid black;
        }
    </style>
    <body>
        <table>
            <tr>
                <th>Username</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Address</th>
                <th>Email</th>
                <th>Contact Number</th>
            </tr>
            <?php
                $query = "SELECT username, first_name, last_name, address, email, contact_number FROM user";
                $result = mysqli_query($db, $query);
                if (mysqli_num_rows($result) > 0){
                    while ($row = mysqli_fetch_assoc($result)){        
            ?>          
                        <tr>
                        <?php foreach ($row as $val) { ?>
                            <td><?php echo $val; ?></td>
                        <?php } ?>
                        </tr>
            <?php
                    }
                } else {
                    echo "0 results";
                }
            ?>
        </table>
    </body>
</html>