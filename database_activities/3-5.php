<?php
    session_start();
?>
<!DOCTYPE HTML>
<html>
    <head>
        <title>Database 3-5</title>
    </head>
    <body>
        <form method="POST">
            <button type="submit" name="logout">Log out</button>
        </form>
        <?php
            if (isset($_POST['logout'])) {
                unset($_SESSION['username']);
                echo "<script>alert('Logout Successfully')</script>";
            }
        ?>
    </body>
</html>