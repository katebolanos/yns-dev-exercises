<?php
    session_start();
?>
<!DOCTYPE HTML>
<html>
    <head>
        <title>HTML & PHP 1-9</title>
    </head>
    <body>
        <form method="POST">
            <button type="submit" name="logout">Log out</button>
        </form>
        <?php
            if (isset($_POST['logout'])) {
                echo "Logout Successfully";
                unset($_SESSION['username']);
            }
        ?>
    </body>
</html>