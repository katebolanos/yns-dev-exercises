<!DOCTYPE HTML>
<html>
    <head>
        <title>HTML & PHP 1-10</title>
    </head>
    <body>
        <form method= "POST" enctype="multipart/form-data">
            Upload Image: <input type= "file" name="image" id="picture">
            <button type="submit" name="submit">Submit</button>
        </form>
        <?php 
            if (isset($_POST['submit'])){
                $target_dir = "uploads/";
                $target_file = $target_dir . basename($_FILES["image"]["name"]);
                $uploadOk = 1;
                $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
                if(isset($_POST["submit"])) {
                    $check = getimagesize($_FILES["image"]["tmp_name"]);
                    if($check !== false) {
                        if (file_exists($target_file)) {
                            echo "Sorry, file already exists.";
                            $uploadOk = 0;
                        } else {
                            echo "Uploaded Sucessfully";
                            $isUploaded = move_uploaded_file($_FILES["image"]["tmp_name"],$target_file);
                            $uploadOk = 1;
                        }
                    } else {
                        echo "File is not an image.";
                        $uploadOk = 0;
                    }
                    
                }
            }
        ?>
    </body>
</html>