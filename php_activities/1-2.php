<!DOCTYPE HTML>
<html>
    <head>
        <title>HTML & PHP 1-2</title>
    </head>
    <body>
        <form method= "POST">
            Num 1: <input type= "text" name= "number1"><br><br>
            Num 2: <input type= "text" name= "number2"><br><br>
            <button type="submit" name="submit">Submit</button>
        </form>
        <?php
            if (isset($_POST['submit'])) {
                $num1 = $_POST['number1'];
                $num2 = $_POST['number2'];
                $sum = $num1 + $num2;
                $difference = $num1 - $num2;
                $product = $num1 * $num2;
                $quotient = $num1 / $num2;
                echo "SUM: " . $sum . "<br>";
                echo "DIFFERENCE: " . $difference . "<br>";
                echo "PRODUCT: " . $product . "<br>";
                echo "QUOTIENT: " . $quotient . "<br>";
            }
        ?>
    </body>
</html>
