<!DOCTYPE HTML>
<html>
    <head>
        <title>HTML & PHP 1-12</title>
    </head>
    <body>
        <table style='border: 1px solid black'>
            <tr>
                <th style='border: 1px solid black'>User Name</th>
                <th style='border: 1px solid black'>Password</th>
                <th style='border: 1px solid black'>First Name</th>
                <th style='border: 1px solid black'>Last Name</th>
                <th style='border: 1px solid black'>Address</th>
                <th style='border: 1px solid black'>Email</th>
                <th style='border: 1px solid black'>Contact Number</th>
            </tr>
            <?php
                $file = fopen("file.csv","r");
                $currentpage = isset($_GET['page']) ? $_GET['page'] : 1;
                $to = $currentpage * 5;
                $from = $to - 5; 
                $ctr = 0;
                while (($line = fgetcsv($file)) !== FALSE) {
                    if ($ctr >= $from && $ctr < $to){
                        echo "<tr style='border: 1px solid black'>";
                        for ($i=0; $i < count($line); $i++) {
                            echo "<td style='border: 1px solid black'>". $line[$i] . "</td>";
                        }
                        echo "</tr>";
                    }
                    $ctr++;
                }
                fclose($file);              
            ?>
        </table>
        <?php
            $fp = file("file.csv");
            $row = count($fp);
            $pages = ceil($row / 5);
            echo "Page:  ";
            for ($i = 1; $i <= $pages; $i++) {
                echo "<a href='1-12.php?page=" . $i ."'>". $i . "</a>" . " | ";
            }
        ?>
    </body>
</html>