<!DOCTYPE HTML>
<html>
    <head>
        <title>HTML & PHP 1-7</title>
    </head>
    <body>
        <form method= "POST">
            First Name: <input type= "text" name= "firstname" ><br>
            Last Name: <input type= "text" name= "lastname" ><br>
            Address: <input type= "text" name= "address" ><br>
            Email: <input type= "text" name= "email"><br>
            Contact Number: <input type= "text"  name= "contactnumber"><br>
            <button type="submit" name="submit">Submit</button><br><br>
        </form>
        <?php
            if (isset($_POST['submit'])){
                $firstname = $_POST['firstname'];
                $lastname = $_POST['lastname'];
                $address = $_POST['address'];
                $email = $_POST['email'];
                $contactnumber = $_POST['contactnumber'];
                if (empty($firstname) || empty($lastname) || empty($address) || empty($email) || empty($contactnumber)){
                    echo "<b>FIELD IS REQUIRED.</b><br>";
                } else {
                    if (!preg_match("/^[a-zA-Z ]*$/", $firstname) || !preg_match("/^[a-zA-Z]*$/", $lastname) || !preg_match("/^[a-zA-Z0-9 ]*$/", $address) || !filter_var($email, FILTER_VALIDATE_EMAIL) || !preg_match("/^[0-9]*$/",$contactnumber)) {
                        echo "<b>INVALID FORMAT</b><br>";
                    } else {
                        echo $firstname . "<br>";
                        echo $lastname . "<br>";
                        echo $address . "<br>";
                        echo $email . "<br>";
                        echo $contactnumber . "<br>";
                    }
                }
            }
        ?>
    </body>
</html>
