<?php
    session_start();
?>
<!DOCTYPE HTML>
<html>
    <head>
        <title>HTML & PHP 1-8</title>
    </head>
    <body>
        <center>Log in</center>
        <form method= "POST">
            User Name: <input type= "text" name= "username"><br>
            Password: <input type= "password" name= "password"><br>
            <button type="submit" name="submit">Log in</button>
        </form>
        <?php
            if (isset($_POST['submit'])){
                $username = $_POST['username'];
                $password = $_POST['password'];
                $lines = file('file.csv');
                $line_number1 = false;
                $line_number2 = false;
                while (list($key, $line) = each($lines) and !$line_number1) {
                    $line_number1 = (strpos($line, $username) !== FALSE );
                    $line_number2 = (strpos($line, $password) !== FALSE );
                }
                if($line_number1 and $line_number2 ){
                    $_SESSION['username'] = $username;
                } else {
                    echo "Incorrect username or password";
                }     
            }
            if(isset($_SESSION['username'])){
                header("Location:../../yns-dev-exercises/php_activities/1-13.php");
            }
        ?>
    </body>
</html>