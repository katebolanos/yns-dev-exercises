<!DOCTYPE HTML>
<html>
    <head>
        <title>HTML & PHP 1-8</title
    </head>
    <body>
        <center>REGISTRATION</center>
        <form method= "POST">
            User Name: <input type= "text" name= "username"><br>
            Password: <input type= "password" name= "password"><br>
            First Name: <input type= "text" name= "firstname"><br>
            Last Name: <input type= "text" name= "lastname"><br>
            Address: <input type= "text" name= "address"><br>
            Email: <input type= "text" name= "email"><br>
            Contact Number: <input type= "text" name= "contactnumber"><br>
            <button type="submit" name="submit">Submit</button>
        </form>
        <?php
            if (isset($_POST['submit'])){
                $username = $_POST['username'];
                $password = $_POST['password'];
                $firstname = $_POST['firstname'];
                $lastname = $_POST['lastname'];
                $address = $_POST['address'];
                $email = $_POST['email']; 
                $contactnumber = $_POST['contactnumber'];
                $filea = fopen('file.csv', 'a');
                $fileo = fopen("file.csv", "r");
                if (empty($username) || empty($password) || empty($firstname) || empty($lastname) || empty($address) || empty($email) || empty($contactnumber)){
                    echo "<b>ALL FIELDS ARE REQUIRED.</b><br>";
                } else {
                    if (!preg_match("/^[a-zA-Z ]*$/", $firstname) || !preg_match("/^[a-zA-Z]*$/", $lastname) || !preg_match("/^[a-zA-Z0-9 ]*$/", $address) || !filter_var($email, FILTER_VALIDATE_EMAIL) || !preg_match("/^[0-9]*$/",$contactnumber)  == TRUE ) {
                        echo "<b>INVALID FORMAT</b><br>";
                    } else {
                        $lines       = file('file.csv');
                        $line_number = false;
                        while (list($key, $line) = each($lines) and !$line_number) {
                            $line_number = (strpos($line, $username) !== FALSE );
                        }
                        if($line_number){
                            echo "USER EXISTS";
                        } else {
                            echo "REGISTERED SUCCESSFULLY";
                            //INSERT DATA TO CSV FILE
                            $output = $username . "," . $password . "," . $firstname . "," . $lastname . "," . $address . "," .  $email . "," . $contactnumber . "\n";
                            $filea = fopen('file.csv', 'a');
                            fwrite($filea, $output);
                        } 
                    }
                } 
                fclose($fileo);
                fclose($filea);
            }
        ?>
    </body>
</html>
