<!DOCTYPE HTML>
<html>
    <head>
        <title>HTML & PHP 1-5</title>
    </head>
    <body>
        <form method= "POST">
            Date: <input type= "datepicker" name= "date"><br><br>
            <button type="submit" name="submit">Submit</button>
        </form>
        <?php
            if (isset($_POST['submit'])) {
                $date = $_POST['date'];
                echo date('l Y-m-d', strtotime($date. ' + 1 days')) . "<br>";
                echo date('l Y-m-d', strtotime($date. ' + 2 days')) . "<br>";
                echo date('l Y-m-d', strtotime($date. ' + 3 days')) . "<br>";
            }
        ?>
    </body>
</html>
