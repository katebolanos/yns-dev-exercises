<!DOCTYPE HTML>
<html>
    <head>
        <title>HTML & PHP 1-4</title>
    </head>
    <body>
        <form method= "POST">
            Num: <input type= "text" name= "number"><br><br>
            <button type="submit" name="submit">Submit</button>
        </form>
        <?php
            if (isset($_POST['submit'])) {
               $num = $_POST['number'];
               for ($i = 1; $i <= $num; $i++) {
                    if($i % 3 & $i % 5 == 0) {
                        echo "Fizz Buzz ";
                    } elseif ($i % 3 == 0) {
                        echo "Fizz ";
                    } elseif ($i % 5 == 0) {
                        echo "Buzz ";
                    } else {
                        echo $i . " ";
                    }
               }
            }
        ?>
    </body>
</html>
